<?php
/**
 * Created by PhpStorm.
 * User: mariusnica
 * Date: 04.05.2016
 * Time: 12:09
 */

class StudentTest extends PHPUnit_Framework_TestCase
{
    /**
     * @expectedException Nica\StudentNumberException
     * @expectedExceptionMessage Invalid student number
     * @dataProvider setStudentBadDataProvider
     */
    public function testSetStudentNumberThrowStudentNumberException($studentNumber)
    {
        $student = new Nica\Student;
        $student->setStudentNumber($studentNumber);
    }

    /**
     * @dataProvider setStudentGoodDataProvider
     */
    public function testSetStudentNumber($studentNumber)
    {
        $student = new Nica\Student;
        $student->setStudentNumber($studentNumber);

        $this->assertEquals(
            $studentNumber,
            $student->getStudentNumber()
        );
    }

    public function testGetInfo()
    {
        $mock = $this->getMock('Nica\Student', array('getAge'));
        $mock->expects($this->once())->method('getAge')->will($this->returnValue('32'));

        $mock->setFirstName('Foo')
            ->setLastName('Bar')
            ->setBirthDate('1984-03-04')
            ->setStudentNumber('SN-12346789')
            ->setCourseName('Informatics');

        $info = $mock->getInfo();

        $this->assertEquals(
            'I am: Foo Bar, Age: 32, Student Number: SN-12346789, Course Name: Informatics',
            $info
        );
    }

    public function setStudentBadDataProvider()
    {
        return array(
            array('SN-1234677-TST'),
            array('1234677-TST'),
            array('1234677'),
            array('SN-1234677456324344')
        );
    }

    public function setStudentGoodDataProvider()
    {
        return array(
            array('SN-1234677'),
            array('SN-1234678910')
        );
    }
}