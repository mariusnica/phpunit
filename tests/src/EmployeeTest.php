<?php


/**
 * Created by PhpStorm.
 * User: mariusnica
 * Date: 04.05.2016
 * Time: 12:09
 */

class EmployeeTest extends PHPUnit_Framework_TestCase
{
    /**
     * @expectedException Nica\EmployeeNumberException
     * @expectedExceptionMessage Invalid employee number
     * @dataProvider setEmployeeNumberBadDataProvider
     */
    public function testSetEmployeeNumberThrowEmployeeNumberException($employeeNumber)
    {
        $employee = new Nica\Employee;
        $employee->setEmployeeNumber($employeeNumber);
    }

    /**
     * @dataProvider setEmployeeNumberGoodDataProvider
     */
    public function testSetEmployeeNumber($employeeNumber)
    {
        $employee = new Nica\Employee;
        $employee->setEmployeeNumber($employeeNumber);

        $this->assertEquals(
            $employeeNumber,
            $employee->getEmployeeNumber()
        );
    }

    /**
     * @expectedException Nica\DateException
     * @expectedExceptionMessage Invalid date
     * @dataProvider setDateHiredBadDataProvider
     */
    public function testSetDateHiredThrowDateException($dateHired)
    {
        $employee = new Nica\Employee;
        $employee->setDateHired($dateHired);
    }

    /**
     * @dataProvider setDateHiredGoodDataProvider
     */
    public function testSetDateHired($dateHired, $format, $expectedFormatedValue)
    {
        $employee = new Nica\Employee;
        $employee->setDateHired($dateHired);

        $this->assertEquals(
            $expectedFormatedValue,
            $employee->getDateHired($format)
        );
    }

    public function testGetInfo()
    {
        $employee = new Nica\Employee();
        $employee->setFirstName('Foo')
            ->setLastName('Bar')
            ->setBirthDate('1984-03-04')
            ->setEmployeeNumber('EN-122234677')
            ->setDateHired('12/15/2015');

        $info = $employee->getInfo();

        $this->assertEquals(
            'I am: Foo Bar, Age: 32, Employee Number: EN-122234677, Date Hired: 15-12-2015',
            $info
        );
    }

    public function setEmployeeNumberBadDataProvider()
    {
        return array(
            array('EN-1234677-TST'),
            array('1234677-TST'),
            array('1234677'),
            array('EN-1234677456324344')
        );
    }

    public function setEmployeeNumberGoodDataProvider()
    {
        return array(
            array('EN-1234677'),
            array('EN-1234678910')
        );
    }

    public function setDateHiredBadDataProvider()
    {
        return array(
            array('Foo 123'),
            array('Fo Bar')
        );
    }

    public function setDateHiredGoodDataProvider()
    {
        return array(
            array('3/4/1984', 'd-m-Y', '04-03-1984'),
            array('1984-12-01', 'd-m-Y', '01-12-1984')
        );
    }
}