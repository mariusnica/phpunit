<?php

/**
 * Created by PhpStorm.
 * User: mariusnica
 * Date: 04.05.2016
 * Time: 12:09
 */
use Nica\ICanWalk;
use Nica\ICanRun;
use Nica\ICanStudy;

class PersonTest extends PHPUnit_Framework_TestCase
{
    /**
     * @expectedException Nica\NameException
     * @expectedExceptionMessage Invalid name
     * @dataProvider setNameBadDataProvider
     */
    public function testSetNameThrowNameException($firstname, $lastName)
    {
        $mock = $this->getMockForAbstractClass('Nica\Person');
        $mock->setFirstName($firstname);
        $mock->setLastName($lastName);
    }

    /**
     * @dataProvider setNameGoodDataProvider
     */
    public function testSetName($firstname, $lastName)
    {
        $mock = $this->getMockForAbstractClass('Nica\Person');
        $mock->setFirstName($firstname);

        $this->assertEquals(
            $firstname,
            $mock->getFirstName()
        );

        $mock->setLastName($lastName);

        $this->assertEquals(
            $lastName,
            $mock->getLastName()
        );
    }

    /**
     * @expectedException Nica\DateException
     * @expectedExceptionMessage Invalid date
     * @dataProvider setBirthdateBadDataProvider
     */
    public function testSetBirthdateThrowDateException($birthdate)
    {
        $mock = $this->getMockForAbstractClass('Nica\Person');
        $mock->setBirthdate($birthdate);
    }

    /**
     * @dataProvider setBirthdateGoodDataProvider
     */
    public function testSetBirthdate($birthdate, $expectedValue, $format='d-m-Y')
    {
        $mock = $this->getMockForAbstractClass('Nica\Person');
        $mock->setBirthdate($birthdate);

        $this->assertEquals(
            $expectedValue,
            $mock->getBirthdate($format)
        );
    }

    /**
     * @expectedException Nica\DateException
     * @expectedExceptionMessage Invalid date
     */
    public function testGetAgeThrowDateException()
    {
        $mock = $this->getMockForAbstractClass('Nica\Person');
        $mock->getAge();
    }

    public function testGetAge()
    {
        $mock = $this->getMockForAbstractClass('Nica\Person');
        $mock->setBirthDate('1984-03-04');
        $age = $mock->getAge();

        $this->assertEquals(
            (new \DateTime('1984-03-04'))
                ->diff(new \DateTime('now'))
                ->y,
            $age
        );
    }

    public function testGetInfo()
    {
        /*
        $arguments = array(),$mockClassName = '',
        $callOriginalConstructor = true,
        $callOriginalClone = true,
        $callAutoload = true,
        $mockedMethods = array()
         */
        $mock = $this->getMockForAbstractClass(
            'Nica\Person', array(), '', true, true, true, array('getAge')
        );
        $mock->expects($this->once())->method('getAge')->will($this->returnValue('32'));
        $mock->setFirstName('Foo');
        $mock->setLastName('Bar');

        $info = $mock->getInfo();

        $this->assertEquals(
            'I am: Foo Bar, Age: 32',
            $info
        );
    }

    public function testWalking()
    {
        $mock = $this->getMockForAbstractClass(
            'Nica\Person'
        );
        $mock->walk();

        $this->assertEquals(ICanWalk::walk, $mock->getActivity());
    }

    public function testRunning()
    {
        $mock = $this->getMockForAbstractClass(
            'Nica\Person'
        );
        $mock->walk();
        $mock->run();

        $this->assertEquals(ICanRun::run, $mock->getActivity());
    }

    public function testStudying()
    {
        $mock = $this->getMockForAbstractClass(
            'Nica\Person'
        );
        $mock->walk();
        $mock->run();
        $mock->study();

        $this->assertEquals(ICanStudy::study, $mock->getActivity());
    }

    public function testOutput()
    {
        $mock = $this->getMockForAbstractClass(
            'Nica\Person'
        );

        $this->expectOutputString(PHP_EOL."Foo Bar output test");
        $mock->output("Foo Bar output test");
    }

    public function setNameBadDataProvider()
    {
        return array(
            array('Foo 123', 'Bar'),
            array('Foo', 'Bar 123'),
            array('', 'Bar 123'),
        );
    }

    public function setNameGoodDataProvider()
    {
        return array(
            array('Foo', 'Bar'),
            array('Foo\'Bar', 'Bar'),
            array('Foo', 'Bar-Bar'),
            array('', 'Bar'),
        );
    }

    public function setBirthdateBadDataProvider()
    {
        return array(
            array(''),
            array('Foo Bar'),
        );
    }

    public function setBirthdateGoodDataProvider()
    {
        return array(
            array('1984-03-04', '04-03-1984'),
        );
    }
}

