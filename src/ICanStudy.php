<?php
/**
 * Created by PhpStorm.
 * User: mariusnica
 * Date: 05.05.2016
 * Time: 17:41
 */

namespace Nica;


interface ICanStudy
{
    const study = 'I am studying...';

    public function study();
}