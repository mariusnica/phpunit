<?php

namespace Nica;

/**
 * Created by PhpStorm.
 * User: mariusnica
 * Date: 03.05.2016
 * Time: 17:25
 */
abstract class Person implements ICanWalk, ICanRun, ICanStudy
{
    /**
     * @var string
     */
    protected $firstName;

    /**
     * @var string
     */
    protected $lastName;

    /**
     * @var \DateTime
     */
    protected $birthDate;

    /**
     * @var array
     */
    protected $activity = [];

    /**
     *
     */
    public function walk()
    {
        array_push($this->activity, ICanWalk::walk);
    }

    /**
     *
     */
    public function run()
    {
        array_push($this->activity, ICanRun::run);
    }

    /**
     *
     */
    public function study()
    {
        array_push($this->activity, ICanStudy::study);
    }

    /**
     * @return string
     * @throws DateException
     */
    public function getInfo()
    {
        return "I am: {$this->getFirstName()} {$this->getLastName()}, Age: {$this->getAge()}";
    }

    /**
     * @return int
     * @throws DateException
     */
    public function getAge(){
        if (empty($this->birthDate) || !$this->birthDate instanceof \DateTime) {
            throw new DateException('Invalid date');
        }

        return
            $this->getBirthDate()
                ->diff(new \DateTime('now'))
                ->y;
    }

    /**
     * @param $value
     * @throws NameException
     */
    public function setFirstName($value)
    {
        $this->validateName($value, true);

        $this->firstName = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param $value
     * @throws NameException
     */
    public function setLastName($value)
    {
        $this->validateName($value, false);

        $this->lastName = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param $value
     * @throws DateException
     */
    public function setBirthDate($value)
    {
        $this->birthDate = $this->validateDate($value);

        return $this;
    }

    /**
     * @param string|null $format
     * @return \DateTime|string
     */
    public function getBirthDate($format=null)
    {
        return
            !empty($this->birthDate) && $this->birthDate instanceof \DateTime && !empty($format)?
                $this->birthDate->format($format) :
                $this->birthDate;
    }

    /**
     * @return string
     */
    public function getActivity()
    {
        return $this->activity ? array_pop($this->activity) : '';
    }

    /**
     * @param string $output
     */
    public function output($output)
    {
        echo PHP_EOL.$output;
    }

    /**
     * @param $value
     * @param bool|false $canBeEmpty - Used to skipp empty check when needed
     * @throws NameException
     */
    protected function validateName($value, $canBeEmpty=false)
    {
        if ((empty($value) && !$canBeEmpty)
            || (!empty($value) && !preg_match("/^[a-zA-Z'-]+$/", $value))
        ) {
            throw new NameException("Invalid name");
        }
    }

    /**
     * @param $value
     * @return \DateTime
     * @throws DateException
     */
    protected function validateDate($value)
    {
        if (empty($value)) {
            throw new DateException("Invalid date");
        }

        try {
            $date = new \DateTime($value);
        } catch (\Exception $e) {
            throw new DateException("Invalid date");
        }

        return $date;
    }
}