<?php

namespace Nica;

class Student extends Person
{
    /**
     * @var string SN-xxxxx
     */
    public $studentNumber;

    /**
     * @var string
     */
    public $courseName;

    /**
     * @return string
     */
    public function getInfo(){
        return
            parent::getInfo()
            . ", Student Number: {$this->studentNumber}, Course Name: {$this->courseName}"
            . ($this->getActivity() ? ', ' . $this->getActivity() : '');
    }

    /**
     * @param $value
     * @return $this
     */
    public function setStudentNumber($value)
    {
        $this->validateStudentNumber($value);

        $this->studentNumber = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getStudentNumber()
    {
        return $this->studentNumber;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setCourseName($value)
    {
        $this->courseName = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getCourseName()
    {
        return $this->courseName;
    }

    /**
     * @param $value
     * @throws StudentNumberException
     */
    protected function validateStudentNumber($value)
    {
        if (!preg_match("/^SN-[0-9]{1,11}+$/", $value)) {
            throw new StudentNumberException("Invalid student number");
        }
    }
}
