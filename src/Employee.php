<?php

namespace Nica;

class Employee extends Person
{
    /**
     * @var string EN-xxxx
     */
    public $employeeNumber;

    /**
     * @var \DateTime
     */
    public $dateHired;

    /**
     * @return string
     */
    public function getInfo()
    {
        return
            parent::getInfo()
            . ", Employee Number: {$this->getEmployeeNumber()}, Date Hired: {$this->getDateHired('d-m-Y')}"
            . ($this->getActivity() ? ', ' . $this->getActivity() : '');
    }

    /**
     * @param $value
     * @return $this
     * @throws EmployeeNumberException
     */
    public function setEmployeeNumber($value)
    {
        $this->validateEmployeeNumber($value);

        $this->employeeNumber = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmployeeNumber()
    {
        return $this->employeeNumber;
    }

    /**
     * @param $value
     * @return $this
     * @throws DateException
     */
    public function setDateHired($value)
    {
        $this->dateHired = $this->validateDate($value);

        return $this;
    }

    /**
     * @param null $format
     * @return \DateTime|string
     */
    public function getDateHired($format=null)
    {
        return
            !empty($this->dateHired) && $this->dateHired instanceof \DateTime && !empty($format)?
                $this->dateHired->format($format) :
                $this->dateHired;
    }

    /**
     * @param $value
     * @throws EmployeeNumberException
     */
    protected function validateEmployeeNumber($value)
    {
        if (!preg_match("/^EN-[0-9]{1,11}+$/", $value)) {
            throw new EmployeeNumberException("Invalid employee number");
        }
    }
}