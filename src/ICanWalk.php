<?php
/**
 * Created by PhpStorm.
 * User: mariusnica
 * Date: 05.05.2016
 * Time: 16:53
 */

namespace Nica;


interface ICanWalk
{
    const walk = 'I am walking...';

    public function walk();
}